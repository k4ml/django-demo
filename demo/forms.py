from django import forms
from demo.models import Customer

class CustomerForm(forms.ModelForm):
    name = forms.ModelChoiceField(Customer.objects.all())

    class Meta:
        model = Customer
        fields = ('name', 'email')


from django.shortcuts import render
from demo.forms import CustomerForm

def index(request):
    form = CustomerForm()
    return render(request, 'index.html', {'form': form})
